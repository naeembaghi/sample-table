import type { Row } from './types';

type NodeType = {
  left?: NodeType;
  right?: NodeType;
  value: Row;
};

class BST {
  root?: NodeType;

  insert = (data: Row) => {
    const newNode: NodeType = {
      value: data,
      left: undefined,
      right: undefined,
    };
    if (!this.root) {
      this.root = newNode;
    } else {
      this.insertNode(this.root, newNode);
    }
  };

  insertNode = (root: NodeType, newNode: NodeType) => {
    if (newNode.value.date > root.value.date) {
      if (!root.left) {
        root.left = newNode;
      } else {
        this.insertNode(root.left, newNode);
      }
    } else {
      if (!root.right) {
        root.right = newNode;
      } else {
        this.insertNode(root.right, newNode);
      }
    }
  };

  search = (date: any) => {
    if (!this.root) {
      return null;
    } else {
      return this.searchNode(this.root, date);
    }
  };

  searchMultiple = (date: any) => {
    if (!this.root) {
      return [];
    }
    const result = [];
    let found = this.searchNode(this.root, date);
    while (found) {
      result.push(found);
      found = this.searchNode(found.right, date);
    }
    return result;
  };

  searchNode = (root: any, value: any): any => {
    if (!root) {
      return null;
    }
    if (value > root.value.date) {
      return this.searchNode(root.left, value);
    } else if (value < root.value.date) {
      return this.searchNode(root.right, value);
    } else {
      return root;
    }
  };
}

export default BST;
