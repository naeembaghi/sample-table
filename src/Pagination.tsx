import React from 'react';

const Pagination = ({
  page,
  onChange,
  maxPage,
}: {
  page: number;
  onChange: (newPage: number) => void;
  maxPage: number;
}) => {
  return (
    <div className="pagination-container">
      {page > 1 && (
        <div className="page" onClick={() => onChange(page - 2)}>
          {page - 1}
        </div>
      )}
      {page > 0 && (
        <div className="page" onClick={() => onChange(page - 1)}>
          {page}
        </div>
      )}
      <div className="page current">{page + 1}</div>
      {page < maxPage - 2 && (
        <div className="page" onClick={() => onChange(page + 1)}>
          {page + 2}
        </div>
      )}
      {page < maxPage - 3 && (
        <div className="page" onClick={() => onChange(page + 2)}>
          {page + 3}
        </div>
      )}
    </div>
  );
};

export default Pagination;
