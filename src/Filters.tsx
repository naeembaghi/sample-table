import React from 'react';
import type { Filter } from './types';
import useLocalizer from './localizer/useLocalizer';

const Filters = ({
  filters,
  onChange,
}: {
  filters: Filter;
  onChange: (filters: Filter) => void;
}) => {
  const { getText } = useLocalizer();
  return (
    <div className="filters">
      <div className="filter-item">
        <label htmlFor="name">{getText('name')}</label>
        <input
          id="name"
          type="text"
          value={filters.name}
          onChange={(event) =>
            onChange({ ...filters, name: event.target.value })
          }
        />
      </div>
      <div className="filter-item">
        <label htmlFor="date">{getText('change-date')}</label>
        <input
          id="date"
          type="date"
          value={filters.date}
          onChange={(event) =>
            onChange({ ...filters, date: event.target.value })
          }
        />
      </div>
      <div className="filter-item">
        <label htmlFor="title">{getText('ad-name')}</label>
        <input
          id="title"
          type="text"
          value={filters.title}
          onChange={(event) =>
            onChange({ ...filters, title: event.target.value })
          }
        />
      </div>
      <div className="filter-item">
        <label htmlFor="field">{getText('field-name')}</label>
        <input
          id="field"
          type="text"
          value={filters.field}
          onChange={(event) =>
            onChange({ ...filters, field: event.target.value })
          }
        />
      </div>
      <div className="filter-item">
        <label htmlFor="oldValue">{getText('old-value')}</label>
        <input
          id="oldValue"
          type="text"
          value={filters.oldValue}
          onChange={(event) =>
            onChange({ ...filters, oldValue: event.target.value })
          }
        />
      </div>
      <div className="filter-item">
        <label htmlFor="newValue">{getText('new-value')}</label>
        <input
          id="newValue"
          type="text"
          value={filters.newValue}
          onChange={(event) =>
            onChange({ ...filters, newValue: event.target.value })
          }
        />
      </div>
    </div>
  );
};

export default Filters;
