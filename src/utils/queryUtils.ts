import type { Filter, ColumnName } from '../types';

const parseQueryString = (): any => {
  // if the query string is NULL or undefined
  const queryString = window.location.search.substring(1);

  const params: any = {};

  const queries = queryString.split('&');

  queries.forEach((indexQuery: string) => {
    const indexPair = indexQuery.split('=');

    const queryKey = decodeURIComponent(indexPair[0]);
    const queryValue = decodeURIComponent(
      indexPair.length > 1 ? indexPair[1] : ''
    );

    params[queryKey] = queryValue;
  });

  return params;
};

const updateQueryParams = (queryParams: any) => {
  if (!queryParams) return;
  let search = '?';
  Object.keys(queryParams).forEach((each, idx) => {
    if (idx !== 0) search += '&';
    search += `${each}=${queryParams[each]}`;
  });
  window.history.pushState({}, '', search);
};

const updateParamsWithFilters = (filters: Filter) => {
  const queryParams = parseQueryString();
  if (filters.name?.length > 0) {
    queryParams.name = filters.name;
  } else {
    delete queryParams.name;
  }
  if (filters.date?.length > 0) {
    queryParams.date = filters.date;
  } else {
    delete queryParams.date;
  }
  if (filters.title?.length > 0) {
    queryParams.title = filters.title;
  } else {
    delete queryParams.title;
  }
  if (filters.field?.length > 0) {
    queryParams.field = filters.field;
  } else {
    delete queryParams.field;
  }
  if (filters.oldValue?.length > 0) {
    queryParams.oldValue = filters.oldValue;
  } else {
    delete queryParams.oldValue;
  }
  if (filters.newValue?.length > 0) {
    queryParams.newValue = filters.newValue;
  } else {
    delete queryParams.newValue;
  }
  updateQueryParams(queryParams);
};

const updateParamsWithSort = (sortColumn: ColumnName, isAsc: boolean) => {
  const queryParams = parseQueryString();
  queryParams.sortColumn = sortColumn;
  queryParams.isAsc = isAsc;
  updateQueryParams(queryParams);
};

const getFilters = (): Filter => {
  const queryParams = parseQueryString();
  return {
    name: queryParams.name || '',
    date: queryParams.date || '',
    title: queryParams.title || '',
    field: queryParams.field || '',
    oldValue: queryParams.oldValue || '',
    newValue: queryParams.newValue || '',
  };
};

const getSort = (): { sortColumn: ColumnName; isAsc: boolean } => {
  const queryParams = parseQueryString();
  return {
    sortColumn: queryParams.sortColumn || 'name',
    isAsc: queryParams.isAsc?.length > 0 ? queryParams.isAsc === 'true' : true,
  };
};

export default {
  parseQueryString,
  updateParamsWithFilters,
  updateParamsWithSort,
  getFilters,
  getSort,
};
