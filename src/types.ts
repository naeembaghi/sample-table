export type Row = {
  name: string;
  date: string;
  title: string;
  field: string;
  old_value: string | number;
  new_value: string | number;
};

export type Filter = {
  name: string;
  date: string;
  title: string;
  field: string;
  oldValue: string;
  newValue: string;
};

export type ColumnName =
  | 'name'
  | 'date'
  | 'title'
  | 'field'
  | 'old_value'
  | 'new_value';
