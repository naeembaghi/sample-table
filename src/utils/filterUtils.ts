import type { Filter, Row } from '../types';

const applyFilters = (
  filters: Filter,
  data: Array<Row>,
  bst: any
): Array<Row> => {
  let rows = [...data];
  if (filters.name?.length > 0) {
    rows = rows.filter(
      (each: Row) => each.name?.toString().indexOf(filters.name) > -1
    );
  }
  if (filters.date?.length > 0) {
    rows = bst.searchMultiple(filters.date).map((each: any) => each.value);
  }
  if (filters.title?.length > 0) {
    rows = rows.filter(
      (each: Row) => each.title?.toString().indexOf(filters.title) > -1
    );
  }
  if (filters.field?.length > 0) {
    rows = rows.filter(
      (each: Row) => each.field?.toString().indexOf(filters.field) > -1
    );
  }
  if (filters.oldValue?.length > 0) {
    rows = rows.filter(
      (each: Row) => each.old_value?.toString().indexOf(filters.oldValue) > -1
    );
  }
  if (filters.newValue?.length > 0) {
    rows = rows.filter(
      (each: Row) => each.new_value?.toString().indexOf(filters.newValue) > -1
    );
  }
  return rows;
};

export default { applyFilters };
