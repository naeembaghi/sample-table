import React from 'react';
import type { ColumnName } from './types';

const Sort = ({
  name,
  onSort,
  sortColumn,
  isAsc,
}: {
  name: ColumnName;
  onSort: (name: ColumnName, isAsc: boolean) => void;
  sortColumn: ColumnName;
  isAsc: boolean;
}) => {
  return (
    <div className="sort-block">
      {(sortColumn !== name || !isAsc) && (
        <img
          alt={`sort-asc-${name}`}
          src="/up_arrow.png"
          className="sort asc"
          onClick={() => onSort(name, true)}
        />
      )}
      {sortColumn === name && isAsc && (
        <img
          alt={`sort-desc-${name}`}
          src="/up_arrow.png"
          className="sort"
          onClick={() => onSort(name, false)}
        />
      )}
    </div>
  );
};

export default Sort;
