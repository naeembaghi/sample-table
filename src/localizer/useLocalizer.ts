import bundle from '../bundle.json';

const useLocalizer = () => {
  const test: { [key: string]: string } = bundle;
  const getText = (str: string): string => test[str];
  return { getText };
};

export default useLocalizer;
