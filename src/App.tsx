import React, { useState, useEffect } from 'react';
import './App.css';
import Filters from './Filters';
import data from './data.json';
import type { Row, ColumnName, Filter } from './types';
import filterUtils from './utils/filterUtils';
import queryUtils from './utils/queryUtils';
import useLocalizer from './localizer/useLocalizer';
import sortUtils from './utils/sortUtils';
import Pagination from './Pagination';
import Table from './Table';
import BST from './BST';

const App = () => {
  const [sortColumn, setSortColumn] = useState<ColumnName>('name');
  const [isAsc, setIsAsc] = useState(true);
  const [staredRows, setStaredRows] = useState<Array<number>>([]);
  const [bst, setBst] = useState<any>();
  const [filters, setFilters] = useState<Filter>({
    name: '',
    date: '',
    title: '',
    field: '',
    oldValue: '',
    newValue: '',
  });
  const [page, setPage] = useState(0);
  const { getText } = useLocalizer();

  useEffect(() => {
    // load stared rows
    const array = localStorage.getItem('staredRows');
    if (array && array.length > 0) {
      setStaredRows(array.split(',').map((each) => Number(each)));
    }

    // load filters
    setFilters(queryUtils.getFilters());

    // load sorted column and direction
    const sortResult = queryUtils.getSort();
    setSortColumn(sortResult.sortColumn);
    setIsAsc(sortResult.isAsc);

    // configure BST
    let rows: Array<Row> = data;
    const newBst = new BST();
    rows.forEach((row) => newBst.insert(row));
    setBst(newBst);
  }, []);

  let rows: Array<Row> = data;
  // apply filters
  const filteredData = filterUtils.applyFilters(filters, rows, bst);

  // handle pagination
  const maxPage = filteredData.length / 1000;
  if (rows.length > 1000) {
    if (page > maxPage) setPage(Math.floor(maxPage));
    rows = filteredData.slice(page * 1000, (page + 1) * 1000);
  }

  // sort data
  sortUtils.sort(rows, sortColumn, isAsc);

  const setSort = (col: ColumnName, asc: boolean) => {
    setSortColumn(col);
    setIsAsc(asc);
    queryUtils.updateParamsWithSort(col, asc);
  };

  const updateStars = (id: number) => {
    const cloned = [...staredRows];
    const arrayIndex = cloned.indexOf(id);
    if (arrayIndex < 0) cloned.push(id);
    else {
      cloned.splice(arrayIndex, 1);
    }
    setStaredRows(cloned);
    localStorage.setItem('staredRows', cloned.toString());
  };

  const updateFilters = (filters: Filter) => {
    setFilters(filters);
    queryUtils.updateParamsWithFilters(filters);
  };

  return (
    <div className="app">
      <Filters filters={filters} onChange={updateFilters} />
      <Pagination page={page} onChange={setPage} maxPage={maxPage} />
      {filteredData.length > 1000 && (
        <div className="warning">{getText('sort-warning')}</div>
      )}
      <Table
        rows={rows}
        sortColumn={sortColumn}
        isAsc={isAsc}
        setSort={setSort}
        staredRows={staredRows}
        updateStars={updateStars}
      />
    </div>
  );
};

export default App;
