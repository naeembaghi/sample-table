import type { Row, ColumnName } from '../types';

const sort = (rows: Array<Row>, sortColumn: ColumnName, isAsc: boolean) => {
  if (rows.length > 1000) return;
  rows.sort((first: Row, second: Row) =>
    isAsc
      ? first[sortColumn] > second[sortColumn]
        ? 1
        : first[sortColumn] < second[sortColumn]
        ? -1
        : 0
      : first[sortColumn] > second[sortColumn]
      ? -1
      : first[sortColumn] < second[sortColumn]
      ? 1
      : 0
  );
};

export default { sort };
