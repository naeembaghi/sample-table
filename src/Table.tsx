import React from 'react';
import useLocalizer from './localizer/useLocalizer';
import Sort from './Sort';
import type { Row, ColumnName } from './types';

const Table = ({
  rows,
  sortColumn,
  isAsc,
  setSort,
  staredRows,
  updateStars,
}: {
  rows: Array<Row>;
  sortColumn: ColumnName;
  isAsc: boolean;
  setSort: (columnName: ColumnName, isAsc: boolean) => void;
  staredRows: Array<number>;
  updateStars: (newStar: number) => void;
}) => {
  const { getText } = useLocalizer();
  return (
    <div className="table-container">
      <table>
        <thead>
          <tr>
            <th style={{ minWidth: 50 }}></th>
            <th style={{ minWidth: 50 }}>{getText('row')}</th>
            <th style={{ minWidth: 100 }}>
              {getText('name')}
              <Sort
                name="name"
                onSort={setSort}
                sortColumn={sortColumn}
                isAsc={isAsc}
              />
            </th>
            <th style={{ width: 100, minWidth: 100 }}>
              {getText('change-date')}
              <Sort
                name="date"
                onSort={setSort}
                sortColumn={sortColumn}
                isAsc={isAsc}
              />
            </th>
            <th style={{ minWidth: 100 }}>
              {getText('ad-name')}
              <Sort
                name="title"
                onSort={setSort}
                sortColumn={sortColumn}
                isAsc={isAsc}
              />
            </th>
            <th style={{ minWidth: 120 }}>
              {getText('field-name')}
              <Sort
                name="field"
                onSort={setSort}
                sortColumn={sortColumn}
                isAsc={isAsc}
              />
            </th>
            <th style={{ minWidth: 120 }}>
              {getText('old-value')}
              <Sort
                name="old_value"
                onSort={setSort}
                sortColumn={sortColumn}
                isAsc={isAsc}
              />
            </th>
            <th style={{ minWidth: 120 }}>
              {getText('new-value')}
              <Sort
                name="new_value"
                onSort={setSort}
                sortColumn={sortColumn}
                isAsc={isAsc}
              />
            </th>
          </tr>
        </thead>
        <tbody>
          {rows.map((each: any, idx: number) => (
            <tr key={each.id}>
              <td>
                <input
                  type="checkbox"
                  checked={staredRows.indexOf(each.id) > -1}
                  onChange={() => updateStars(each.id)}
                />
              </td>
              <td>{idx + 1}</td>
              <td>{each.name}</td>
              <td>{each.date}</td>
              <td>{each.title}</td>
              <td>{each.field}</td>
              <td>{each.old_value}</td>
              <td>{each.new_value}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
